import {Injectable} from '@nestjs/common';
import type {HTTPError} from "got";

import {InvalidGitHubRepositoryException, RepositoryNotGitHubException} from "./exceptions";
import {ProjectDatabaseService} from "../project-database/project-database.service";
import {Project} from "./project.entity";
import {Language} from "./language.entity";

@Injectable()
export class ProjectService {
  constructor(private readonly projectDatabaseService: ProjectDatabaseService) {
  }

  public async create(key: string, name: string, description: string, repositoryUrl: string): Promise<Project> {
    const {default: got, HTTPError} = await (eval('import("got")') as Promise<typeof import("got")>)

    try {
      const url = new URL(repositoryUrl)
      if (url.hostname !== "github.com") throw new RepositoryNotGitHubException()

      const project = new Project(key, name, description, repositoryUrl)
      const data = await got.get(`https://api.github.com/repos${url.pathname}/languages`).json<Record<string, number>>();

      Object.keys(data).forEach((name) => {
        if (!project.languages) project.languages = []
        project.languages.push(new Language(name))
      })

      // add auth guard

      return this.projectDatabaseService.create(project)
    } catch (e) {
      if (e instanceof HTTPError) {
        const httpError = e as HTTPError
        if (httpError.response.statusCode === 404) {
          throw new InvalidGitHubRepositoryException()
        }
      }

      throw e
    }
  }
}
