import {Test, TestingModule} from '@nestjs/testing';
import {PostgresAuthDatabaseService} from './auth-database.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../../user/user.entity";
import {AuthDatabaseService} from "../auth-database.service";

describe('PostgresAuthDatabaseService', () => {
  let service: PostgresAuthDatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forFeature([User])],
      providers: [{
        provide: AuthDatabaseService,
        useClass: PostgresAuthDatabaseService
      }],
      exports: [{
        provide: AuthDatabaseService,
        useClass: PostgresAuthDatabaseService
      }],
    }).compile();

    service = module.get<PostgresAuthDatabaseService>(PostgresAuthDatabaseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
