import {Column, Entity, JoinTable, ManyToMany, PrimaryColumn} from "typeorm";
import {Project} from "../project/project.entity";

@Entity()
export class User {
  constructor(email: string, username: string, firstName: string, lastName: string, password: string) {
    this.email = email
    this.username = username
    this.firstName = firstName
    this.lastName = lastName
    this.password = password
  }

  @PrimaryColumn()
  public username: string

  @Column({nullable: false})
  public email: string;

  @Column({nullable: false})
  public firstName: string;

  @Column({nullable: false})
  public lastName: string;

  @Column({nullable: false})
  public password: string;

  @JoinTable()
  @ManyToMany(() => Project, project => project.users)
  public projects: Project[]
}
