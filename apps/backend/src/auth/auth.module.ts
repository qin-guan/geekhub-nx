import {Module} from '@nestjs/common';
import {ConfigModule} from "@nestjs/config";
import {AuthDatabaseModule} from "../auth-database/auth-database.module";
import {AuthService} from "./auth.service";

@Module({
  imports: [ConfigModule, AuthDatabaseModule],
  providers: [AuthService],
  exports: [AuthService]
})
export class AuthModule {
}
