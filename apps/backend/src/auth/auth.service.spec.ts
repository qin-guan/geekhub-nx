import {Test, TestingModule} from '@nestjs/testing';
import {AuthService} from './auth.service';
import {ConfigModule} from "@nestjs/config";
import {AuthDatabaseModule} from "../auth-database/auth-database.module";

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule, AuthDatabaseModule],
      providers: [AuthService],
      exports: [AuthService]
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
