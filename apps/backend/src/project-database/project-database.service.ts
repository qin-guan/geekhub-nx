import {Injectable} from '@nestjs/common';
import {Language} from "../project/language.entity";
import {Project} from "../project/project.entity";

@Injectable()
export abstract class ProjectDatabaseService {
  abstract findLanguageByName(name: string): Promise<Language>

  abstract create(project: Project): Promise<Project>
}
