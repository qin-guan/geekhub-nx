import {Test, TestingModule} from '@nestjs/testing';
import {PostgresProjectDatabaseService} from "./project-database.service";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Project} from "../../project/project.entity";
import {Language} from "../../project/language.entity";
import {ProjectDatabaseService} from "../project-database.service";

describe('PostgresProjectDatabaseService', () => {
  let service: PostgresProjectDatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forFeature([Project, Language])],
      providers: [{
        provide: ProjectDatabaseService,
        useClass: PostgresProjectDatabaseService
      }],
      exports: [{
        provide: ProjectDatabaseService,
        useClass: PostgresProjectDatabaseService
      }]
    }).compile();

    service = module.get<PostgresProjectDatabaseService>(PostgresProjectDatabaseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
