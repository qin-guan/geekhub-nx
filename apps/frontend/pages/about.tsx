import {Box, Container, Text} from "@mantine/core";
import {Header} from "../components/Header";

export default function About() {
  return (
    <Box>
      <Header/>
      <Container mt={"5rem"}>
        <Text
          weight={700}
          sx={{fontSize: "3rem"}}>
          Make open source
          <Text color={"blue"} sx={{fontSize: "inherit"}}>
            great again?
          </Text>
        </Text>
      </Container>
    </Box>
  )
}
