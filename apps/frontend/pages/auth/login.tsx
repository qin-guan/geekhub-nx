import {useState} from "react";
import {useRouter} from "next/router";

import {useForm} from "@mantine/hooks";
import {Box, Button, Card, Center, Group, PasswordInput, TextInput, Title} from "@mantine/core";

import {Header} from "../../components/Header";
import {ky} from "../../api/ky";
import {useWhoAmI} from "../../hooks/use-who-am-i";

export default function Login() {
  const router = useRouter()

  const {isLoading, data} = useWhoAmI()
  const [loading, setLoading] = useState(false)

  const form = useForm({
    initialValues: {
      username: "",
      password: ""
    },

    validationRules: {
      username: (value) => value.length > 0,
      password: (value) => value.length > 0
    },

    errorMessages: {
      username: "Please enter a valid username",
      password: "Please enter a valid password"
    }
  });

  if (!isLoading && data) {
    router.push("/")
    return null
  }

  const login = async ({username, password}) => {
    setLoading(true)
    try {
      await ky.post("auth/login", {json: {username, password}})
      router.push("/")
    } catch {
      form.setFieldError("password", "Incorrect password D:")
    } finally {
      setLoading(false)
    }
  }

  return (
    <Box sx={{height: "100vh", display: "flex", flexDirection: "column"}}>
      <Header/>
      <Center sx={{flex: 1}}>
        <Card padding={"xl"} sx={{
          minWidth: "300px",
        }}>
          <Title order={3} mb={"sm"}>Login</Title>
          <form onSubmit={form.onSubmit(login)}>
            <Group spacing={"md"} direction={"column"} grow>
              <TextInput
                label={"Username"}
                placeholder="johnappleseed@apple.com"
                error={form.errors["username"]}
                {...form.getInputProps("username")}
              />
              <PasswordInput
                label={"Password"}
                placeholder="Password"
                {...form.getInputProps("password")}
              />
              <Button type={"submit"} loading={loading}>Login</Button>
            </Group>
          </form>
        </Card>
      </Center>
    </Box>
  )
}
