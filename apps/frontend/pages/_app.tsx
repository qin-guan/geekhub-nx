import {AppProps} from 'next/app';
import Head from 'next/head';

import {MantineProvider} from "@mantine/core";
import {QueryClient, QueryClientProvider} from "react-query";

const queryClient = new QueryClient()

export default function _App({Component, pageProps}: AppProps) {
  return (
    <>
      <Head>
        <title>GeekHub</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width"/>
      </Head>
      <main className="app">
        <MantineProvider
          withGlobalStyles
          withNormalizeCSS
          theme={{
            colorScheme: 'dark',
            fontFamily: "IBM Plex Sans",
            fontFamilyMonospace: "Jetbrains Mono",
            headings: {
              fontFamily: "Poppins"
            }
          }}
        >
          <QueryClientProvider client={queryClient}>
            <Component {...pageProps} />
          </QueryClientProvider>
        </MantineProvider>
      </main>
    </>
  );
}
