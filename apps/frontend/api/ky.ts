import _ky from "ky"

const prefixUrl = "http://localhost:3333/api"

export const ky = _ky.create({
  prefixUrl,
  credentials: "include",
  hooks: {
    afterResponse: [
      async (_req, _options, res) => {
        if (res.status === 401) {
          await _ky("auth/refresh", {prefixUrl, credentials: "include"})
        }
      }
    ]
  }
})
